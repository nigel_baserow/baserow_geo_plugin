FROM baserow_backend

USER root

COPY ./plugins/baserow_geo_plugin/ /baserow/plugins/baserow_geo_plugin/
RUN /baserow/plugins/install_plugin.sh --folder /baserow/plugins/baserow_geo_plugin

USER $UID:$GID
