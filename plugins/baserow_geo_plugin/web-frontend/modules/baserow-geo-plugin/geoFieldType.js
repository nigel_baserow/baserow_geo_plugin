import {FieldType} from '@baserow/modules/database/fieldTypes'
import FieldTextSubForm from '@baserow/modules/database/components/field/FieldTextSubForm'
import RowEditFieldText from '@baserow/modules/database/components/row/RowEditFieldText'
import RowCardFieldText from '@baserow/modules/database/components/card/RowCardFieldText'
import {
    genericContainsFilter,
} from '@baserow/modules/database/utils/fieldFilters'

import FunctionalGridViewFieldPoint from '@baserow-geo-plugin/components/FunctionalGridViewFieldPoint'
import GridViewFieldPoint from '@baserow-geo-plugin/components/GridViewFieldPoint'

export class PointFieldType extends FieldType {
    static getType() {
        return 'point'
    }

    getIconClass() {
        return 'globe'
    }

    getName() {
        return  'Point'
    }

    getFormComponent() {
        return FieldTextSubForm
    }

    getGridViewFieldComponent() {
        return GridViewFieldPoint
    }

    getFunctionalGridViewFieldComponent() {
        return FunctionalGridViewFieldPoint
    }

    getRowEditFieldComponent() {
        return RowEditFieldText
    }

    getCardComponent() {
        return RowCardFieldText
    }

    getEmptyValue(field) {
        return field
    }

    getSort(name, order) {
        return (a, b) => {
            const stringA = a[name] === null ? '' : '' + a[name]
            const stringB = b[name] === null ? '' : '' + b[name]

            return order === 'ASC'
                ? stringA.localeCompare(stringB)
                : stringB.localeCompare(stringA)
        }
    }

    getDocsDataType(field) {
        return 'string'
    }

    getDocsDescription(field) {
        return this.app.i18n.t('fieldDocs.text')
    }

    getDocsRequestExample(field) {
        return 'string'
    }

    getContainsFilterFunction() {
        return genericContainsFilter
    }

    canBeReferencedByFormulaField() {
        return true
    }
}
