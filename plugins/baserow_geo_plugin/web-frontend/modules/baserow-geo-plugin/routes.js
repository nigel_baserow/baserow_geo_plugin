import path from 'path'

export const routes = [
    {
        name: 'example',
        path: '/example',
        component: path.resolve(__dirname, 'pages/example.vue'),
    },
]