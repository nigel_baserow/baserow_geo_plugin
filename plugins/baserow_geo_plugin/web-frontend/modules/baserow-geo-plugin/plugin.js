import {PluginNamePlugin} from '@baserow-geo-plugin/plugins'
import {PointFieldType} from '@baserow-geo-plugin/geoFieldType'

export default (context) => {
  const { app } = context
  app.$registry.register('plugin', new PluginNamePlugin(context))
  app.$registry.register('field', new PointFieldType(context))
}
