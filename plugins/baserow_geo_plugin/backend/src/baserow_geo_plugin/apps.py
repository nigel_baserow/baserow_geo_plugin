from django.apps import AppConfig


class PluginNameConfig(AppConfig):
    name = "baserow_geo_plugin"

    def ready(self):
        from baserow.core.registries import plugin_registry
        from .plugins import PluginNamePlugin

        plugin_registry.register(PluginNamePlugin())
        from baserow.contrib.database.fields.registries import field_type_registry

        from .geo_field_type import PointFieldType

        field_type_registry.register(PointFieldType())
