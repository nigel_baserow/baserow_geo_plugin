app_name = "baserow_geo_plugin.api"

from django.urls import re_path

from .views import ExampleView

urlpatterns = [
    re_path(r'example/$', ExampleView.as_view(), name='example'),
]

