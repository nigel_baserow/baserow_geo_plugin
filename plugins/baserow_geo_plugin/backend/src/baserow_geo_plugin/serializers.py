import json
from decimal import Decimal

from django.contrib.gis.geos import GEOSGeometry, GEOSException
from rest_framework.exceptions import ValidationError
from rest_framework_gis.serializers import GeometryField


class GeometryPointFieldSerializerField(GeometryField):

    def to_internal_value(self, value):
        value = {
            "type": "Point",
            "coordinates": [value['lng'], value['lat']]
        }
        value = json.dumps(value)
        return GEOSGeometry(value)

    def to_representation(self, value):
        value = super().to_representation(value)
        if not value:
            return None
        data = {
            "lat": value['coordinates'][1],
            "lng": value['coordinates'][0],
        }
        return data