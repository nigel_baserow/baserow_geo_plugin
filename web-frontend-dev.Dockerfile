FROM baserow_web-frontend

USER root

ARG PLUGIN_BUILD_UID
ENV PLUGIN_BUILD_UID=${PLUGIN_BUILD_UID:-9999}
ARG PLUGIN_BUILD_GID
ENV PLUGIN_BUILD_GID=${PLUGIN_BUILD_GID:-9999}

# If we aren't building as the same user that owns all the files in the base
# image/installed plugins we need to chown everything first.

RUN [ "/bin/bash", "-c", "if [[ $PLUGIN_BUILD_UID != '9999' || $PLUGIN_BUILD_GID != '9999' ]] ; then chown -R $PLUGIN_BUILD_UID:$PLUGIN_BUILD_GID /baserow/; fi" ]
COPY --chown=$PLUGIN_BUILD_UID:$PLUGIN_BUILD_GID ./plugins/baserow_geo_plugin/ /baserow/plugins/baserow_geo_plugin/
RUN /baserow/plugins/install_plugin.sh --folder /baserow/plugins/baserow_geo_plugin --dev


USER $PLUGIN_BUILD_UID:$PLUGIN_BUILD_GID
CMD ["nuxt-dev"]
