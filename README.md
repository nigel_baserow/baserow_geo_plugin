# Baserow Geo Plugin

> This plugin is an example plugin for developers to use as reference. It is not for
> actual users to install currently.

This plugin adds a Point field to Baserow. To do so shows all the ways a Baserow plugin
can do custom installation steps and configure a Baserow instance, for example it:
1. Add extra INSTALLED_APP dependencies to Baserow's backend Django app.
2. Run custom install steps, in this plugin's case to install the postgis extension.
3. Add python and javascript dependencies.
4. Add a basic field type.

# Installing this plugin

1. Create a `Dockerfile`
```dockerfile
FROM registry.gitlab.com/bramw/baserow/ci/baserow:ci-latest-432-update-plugin-boilterplate-and-docs-to-match-new-docker-usage

RUN /baserow/plugins/install_plugin.sh \
    --git https://gitlab.com/nigel_baserow/baserow_geo_plugin.git 
```
3. Now build your custom Baserow with the plugin installed by running:
   `docker build -t my-customized-baserow:1.10.0 .`
4. Finally, you can run your new customized image just like the normal Baserow image:
   `docker run -p 80:80 -v baserow_data:/baserow/data my-customized-baserow:1.10.0`


## Baserow Plugins

A Baserow plugin can be used to extend or change the functionality of Baserow. 
Specifically a plugin is a folder with a `backend` and/or `web-frontend` folder. 

## How to run Baserow Geo Plugin using Docker-compose

A number of different example docker-compose files are provided in this folder:

> It is recommended to use build kit with these compose files, you can do this by
> running:
> ```bash
> export COMPOSE_DOCKER_CLI_BUILD=1
> export DOCKER_BUILDKIT=1
> ```

1. `docker-compose.yml` - This is the simplest compose file that will run the your 
   plugin installed into a single container, use `docker-compose up`.
2. `docker-compose.caddy.yml` - This is a more complex compose file which runs each of
   the Baserow services with your plugin installed in separate containers all behind a
   Caddy reverse proxy.
    1. `docker-compose -f docker-compose.caddy.yml up -d --build`
3. `docker-compose.dev.yml` - This is a development compose file which runs service in 
   a separate container like the `.caddy.yml` above. The images used will be
   the development variants which have dev dependencies installed. Additionally, it will
   mount in the local source code into the containers so for hot code reloading. To
   ensure the mounting of your local code into the contains works correctly make sure
   you run with the following two environment variables set to your uid and gid:
    1. `PLUGIN_BUILD_UID=$(id -u) PLUGIN_BUILD_GID=$(id -g) docker-compose -f docker-compose.dev.yml up -d --build`

## Missing features TODO

1. A templated setup guide in the generated folder itself.
2. Example tests for web-frontend and backend.
3. An equivalent dev.sh
4. Setup instructions for IDEs (vs-code/intellij)
5. A more general and well-defined plugin structure allowing multiple plugins to be
   installed at the same time.
6. Example Gitlab/Github CI integration + instructions to publish plugin to Dockerhub.
