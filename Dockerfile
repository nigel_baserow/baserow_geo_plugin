FROM baserow

COPY ./plugins/baserow_geo_plugin/ /baserow/plugins/baserow_geo_plugin/
RUN /baserow/plugins/install_plugin.sh --folder /baserow/plugins/baserow_geo_plugin
