FROM registry.gitlab.com/bramw/baserow/ci/baserow:ci-latest-432-update-plugin-boilterplate-and-docs-to-match-new-docker-usage

RUN /baserow/plugins/install_plugin.sh \
    --git https://gitlab.com/nigel_baserow/baserow_geo_plugin.git
